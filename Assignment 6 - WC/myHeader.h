/*****************************************************
 *AUTHOR         :  The Oz
 *CLASS          :  CS 1B
 *SECTION        :  TTh:  8am - 11:50am
 *DUE DATE       :  5/12/15
 *****************************************************/

#ifndef MYHEADER_H_
#define MYHEADER_H_

#include <iostream>
#include <iomanip>
#include <string>
#include <cctype>
#include <fstream>
#include <limits>
#include <ios>
#include <cstdlib>
#include <sstream>
#include <string>

#include "Account.h"
#include "SaddlebackBank.h"
using namespace std;


/***********************************************************
 *PrintHeaderToFile
 *	 This function takes care of the class heading so it
 *	 prints the most proper one (lab or assignment)
 *	 -Returns nothing -->This will ouput class heading
 ***********************************************************/
void PrintHeader(ostream& outputF,    // OUT - class header to file
				 string   asName,     // IN  - Assignment name
				 string   programers, // IN  - Programmer(s) name
				 char     asType,     // IN  - Type of project
								      //      (lab or assignment)
				 int      asNum);     // IN  - Project number

/***********************************************************
 *Fill
 *	 This function uses the fill function to fill a whole
 *   line with a certain characeter and then it clears it
 *   for the next one to be written anything.
 *	 -Returns nothing --> the filled line
 ***********************************************************/
string Fill(
		  char fillChar,    //IN  - character to fill the line with
		  int width);       //IN  - width of the line to be filled out


/**********************************************************
*
* FUNCTION ReadInfoFromFile
*	 This function reads the account information from an
*	 input file. After reading the information, it creates a
*	 object depending upon  the account type.
*    Returns -> new account by reference
***********************************************************/

void ReadInfoFromFile(Bank &myBank, //IN/OUT - new account
					  string inFileName, ostream &oFile);    //IN     - input file name

/**********************************************************
* FUNCTION ReadInstructionsFromFile
* 	This function reads the account operations from an
* 	input file. After reading the information, it returns the
* 	instructions by reference, so they can be used in main.
***********************************************************/
void ReadInstructionsFromFile(Date &transferdate,	//IN/OUT - Date of transfer
							  unsigned short &from,	//IN/OUT - Where is the transaction coming from
							  float &transferAmt,	//IN/OUT - size of the transaction
							  string &typeOfAction, //IN/OUT - Type of transaction
							  unsigned short &destination,//IN/OUT - Where is the money going to
							  ifstream &inFile);//IN/OUT - Input file use to read instructions

#endif /* MYHEADER_H_ */
