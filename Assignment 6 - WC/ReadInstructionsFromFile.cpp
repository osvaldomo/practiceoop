/*****************************************************
 *AUTHOR         :  The Oz
 *CLASS          :  CS 1B
 *SECTION        :  TTh:  8am - 11:50am
 *DUE DATE       :  5/14/15
 *****************************************************/

#include "myHeader.h"
#include "SaddlebackBank.h"
#include "Date.h"
#include "Account.h"
#include "Savings.h"
#include "Checking.h"
#include "MoneyMarket.h"

/**********************************************************
* FUNCTION ReadInstructionsFromFile
*_________________________________________________________
* This function reads the account operations from an
* input file. After reading the information, it returns the
* instructions by reference, so they can be used in main.
*_________________________________________________________
* PRE-CONDITIONS
* transferdate  :  Date of transfer
* from    	    :  Where is the transaction coming from
* transferAmt   :  size of the transaction
* typeOfAction  :  Type of transaction
* destination   :  Where is the money going to
* inputFile		:  Input file use to read instructions
*
* POST-CONDITIONS
* This function will create a new Account type (savings,
* checking, or Money Market).
***********************************************************/

void ReadInstructionsFromFile(Date &transferdate,	//IN/OUT - Date of transfer
							  unsigned short &from,	//IN/OUT - Where is the transaction coming from
							  float &transferAmt,	//IN/OUT - size of the transaction
							  string &typeOfAction, //IN/OUT - Type of transaction
							  unsigned short &destination,//IN/OUT - Where is the money going to
							  ifstream &inFile)//IN/OUT - Input file use to read instructions
{
	unsigned short newDay;   //IN - account creation day
	unsigned short newMonth; //IN - account creation month
	unsigned short newYear;  //IN - account creation year

		inFile >> newDay;

		inFile >> newMonth;

		inFile >> newYear;

		inFile >> from;

		inFile >> transferAmt;

		inFile >> typeOfAction;


		if(typeOfAction == "Transfer")
		{
			inFile.ignore(numeric_limits<streamsize> :: max() , '\n');
			inFile >> destination;

		}

		inFile.ignore(numeric_limits<streamsize> :: max() , '\n');
		transferdate.SetDate(newDay, newMonth, newYear);

}
