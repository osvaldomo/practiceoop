/*****************************************************
 *AUTHOR         :  The Oz
 *CLASS          :  CS 1B
 *SECTION        :  TTh:  8am - 11:50am
 *DUE DATE       :  5/14/15
 *****************************************************/

#include "Checking.h"

/**********************************************************************
 * Account :: IsOverdrawn
 *
 *	This method returns true or false if the account is under -200 USD
 *
 *	PRECONDITIONS
 *		NONE
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: true or false
 *********************************************************************/
bool Checking :: IsOverdrawn() const
{

	return GetBalance() < -200;

}

/**********************************************************************
 * Account :: IsNegative
 *
 *	This method returns true or false if the account is under 0 USD
 *
 *	PRECONDITIONS
 *		NONE
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: true or false
 *********************************************************************/
bool Checking :: IsNegative() const
{

	return GetBalance() < 0;

}

/**********************************************************************
 * Account :: GetBalance
 *
 *	This method returns the current balance in the account
 *
 *	PRECONDITIONS
 *		NONE
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: the account balance
 *********************************************************************/
float Checking :: GetBalance() const
{

	return Account :: GetBalance();

}

