/*****************************************************
 *AUTHOR         :  The Oz
 *CLASS          :  CS 1B
 *SECTION        :  TTh:  8am - 11:50am
 *DUE DATE       :  5/12/15
 *****************************************************/

#include "MoneyMarket.h"

/**********************************************************************
 * MoneyMarket :: GetBalance
 *
 *	This method returns the current balance in the account
 *
 *	PRECONDITIONS
 *		NONE
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: the account balance
 *********************************************************************/
float MoneyMarket :: GetBalance() const
{

	return Account :: GetBalance();

}

/**********************************************************************
 * MoneyMarket :: IsEmpty
 *
 *	This method returns if the account has no funds
 *
 *	PRECONDITIONS
 *		NONE
 *
 *	POSTCONDITIONS
 *		true of false
 *
 * RETURNS: true of false
 *********************************************************************/
 bool MoneyMarket :: IsEmpty() const
 {

	return GetBalance() == 0;
 }
