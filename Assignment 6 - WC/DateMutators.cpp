/*****************************************************
 *AUTHOR         :  The Oz
 *CLASS          :  CS 1B
 *SECTION        :  TTh:  8am - 11:50am
 *DUE DATE       :  5/12/15
 *****************************************************/

#include "Date.h"

/**********************************************************************
 * Date :: Date()
 *	This Constructor Initializes class attributes
 *
 *	PRECONDITIONS
 *		NONE
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: nothing - initializes the Date class
 *********************************************************************/
Date :: Date()
{

	time_t now;
	tm *currentTime;
	unsigned short currentYear;   // CALC - stores current year
	unsigned short currentMonth;  // CALC - stores current month
	unsigned short currentDay;    // CALC - stores current day

	now = time(NULL);
	currentTime = localtime(&now);

	currentYear = 1900 + currentTime -> tm_year;
	currentMonth = currentTime -> tm_mon;
	currentDay = currentTime -> tm_mday;

	dateMonth = currentMonth;
	dateDay   = currentDay;
	dateYear  = currentYear;

}

/**********************************************************************
 * Date :: Date(month, day, year)
 *
 *	This Constructor Initializes class attributes specified in main, if
 *	not valid, they are set to default current time
 *
 *	PRECONDITIONS
 *		month :  Month of the year
 *		day   :  day of the month
 *		year  :  current year
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: month, day, and year - initializes the Date class
 *********************************************************************/
Date :: Date(unsigned short 		month,
		 	 unsigned short day,
		 	 unsigned short year)
{

	if(ValidateDay(month, day, year) &&
	   ValidateMonth(month) && ValidateYear(year))
	{

		dateMonth  = month;
		dateDay    = day;
		dateYear   = year;
	}
	else
	{
		cout << "INVALID DATE. Setting time to default......"
		     << endl << endl;

		time_t now;
		tm *currentTime;
		unsigned short currentYear;   // CALC - stores current year
		unsigned short currentMonth;  // CALC - stores current month
		unsigned short currentDay;    // CALC - stores current day

		now = time(NULL);
		currentTime = localtime(&now);

		currentYear = 1900 + currentTime -> tm_year;
		currentMonth = currentTime -> tm_mon;
		currentDay = currentTime -> tm_mday;

		dateMonth = currentMonth;
		dateDay   = currentDay;
		dateYear  = currentYear;
	}
}

/**********************************************************************
 * Date :: ~Date
 *
 *	This Distructor does nothing
 *
 *	PRECONDITIONS
 *		NONE
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: Nothing
 *********************************************************************/
Date :: ~Date()
{

}

/**********************************************************************
 * SetDate
 *	This method sates a date specified by the user
 *
 *	PRECONDITIONS
 *		month : specified month
 *		day   : specified day
 *		year  : specified year
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: Nothing -> sets the date.
 *********************************************************************/
void Date :: SetDate(unsigned short month,
	 	 	 	 	 unsigned short day,
	 	 	 	 	 unsigned short year)
{

	time_t now;
		tm *currentTime;
		unsigned short currentYear;   // CALC - stores current year
		unsigned short currentMonth;  // CALC - stores current month
		unsigned short currentDay;    // CALC - stores current day

		now = time(NULL);
		currentTime = localtime(&now);

		currentYear = 1900 + currentTime -> tm_year;
		currentMonth = currentTime -> tm_mon;
		currentDay = currentTime -> tm_mday;

		if(!ValidateDate(day, month, year))
		{

			dateYear = currentYear;
			dateMonth = currentMonth;
			dateDay = currentDay;

		}
		else
		{

			dateYear = year;
			dateMonth = month;
			dateDay = day;

		}

}

