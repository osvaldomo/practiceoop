/*****************************************************
 *AUTHOR         :  The Oz
 *CLASS          :  CS 1B
 *SECTION        :  TTh:  8am - 11:50am
 *DUE DATE       :  5/14/15
 *****************************************************/

#ifndef MONEYMARKET_H_
#define MONEYMARKET_H_

#include "Savings.h"

class MoneyMarket : public Savings
{

	/*******************************
	 ** CONSTRUCTOR & DESTRUCTORS **
	 *******************************/
public:

	//default constructor
	MoneyMarket();

	//non-dafault constructor
	MoneyMarket(string accntName,
			unsigned short accntNumber,
			Date openDate,
			float balanceAmt,
			string  acctType);

	//destructor
	virtual ~MoneyMarket();

	/***************
	 ** MUTATORS  **
	 ***************/

	//updates account information
	virtual void UpdateAccount(Date lastOpenDate);

	//withdrws from the account
	virtual bool Withdraw(float withdrawAmt,
						  Date lastAccesed);

	//deposits into the account
	virtual void Deposit(float amountIn, Date newDate);

	/****************
	 ** ACCESSORS  **
	 ****************/

	//gets the account balance
	virtual float GetBalance() const;

private:

	//checks is account is empty
	bool IsEmpty() const;

	float interestRate; //CALC - added interest rate

	float withdrawalFee;//CALC - fee in case of withdrawal

};

/***********************************************************************
 * MoneyMarket Class
 *   This class represents a Money Market account object
 *   It manages 2 attributes. interestRate and withdrawalFee
 ***********************************************************************/

/*******************************
 ** CONSTRUCTOR & DESTRUCTORS **
 *******************************/

/***********************************************************************
 ** MoneyMarket();
 **   Constructor; Initializes class attributes
 **   Parameter: None
 **   Returns: Nothing
 ***********************************************************************/

/***********************************************************************
 ** MoneyMarket(float intRate,
				float fee);
 **   Constructor; Initializes class attributes
 **   Parameter: intRate and fee
 **   Returns: Nothing - initializes class attributes
 ***********************************************************************/

/***********************************************************************
 ** ~MoneyMarket();
 **   Destructor; Does not perform any specific function
 **   Parameter: None
 **   Returns: Nothing
 ***********************************************************************/

/***************
 ** MUTATORS  **
 ***************/

/***********************************************************************
 ** Withdraw
 **   Mutator; This function decreases the balance, depending upon the amount
 **   of funds.
 **---------------------------------------------------------------------
 **   Parameter: withdrawAmt, accntNumber, and lastAccessed
 **---------------------------------------------------------------------
 **   Returns: bool if found
 ***********************************************************************/

/***********************************************************************
 ** UpdateAccount
 **   Mutator; This function updates all the information in the
 **   account
 **---------------------------------------------------------------------
 **   Parameter: lastOpenDate
 **---------------------------------------------------------------------
 **   Returns: nothing
 ***********************************************************************/

/***********************************************************************
 ** Deposit
 **   Mutator; This function changes the balance in the account (increases)
 **---------------------------------------------------------------------
 **   Parameter: amountIn
 **---------------------------------------------------------------------
 **   Returns: Nothing
 ***********************************************************************/

/***************
** ACCESSORS **
 ***************/

/***********************************************************************
 ** IsEmpty
 **   Mutator; gets true or false if the account is under or equals $0
 **---------------------------------------------------------------------
 **   Parameter: none
 **---------------------------------------------------------------------
 **   Returns: true or false
 ***********************************************************************/

/***********************************************************************
 ** GetBalance
 **   Mutator; gets the account balance
 **---------------------------------------------------------------------
 **   Parameter: none
 **---------------------------------------------------------------------
 **   Returns: the account balance
 ***********************************************************************/

#endif /* MONEYMARKET_H_ */
