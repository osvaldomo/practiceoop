/*****************************************************
 *AUTHOR         :  The Oz
 *CLASS          :  CS 1B
 *SECTION        :  TTh:  8am - 11:50am
 *DUE DATE       :  5/14/15
 *****************************************************/

#ifndef ACCOUNT_H_
#define ACCOUNT_H_

#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include <sstream>
#include "Date.h"
using namespace std;

class Account
{

public:

	/*******************************
	 ** CONSTRUCTOR & DESTRUCTORS **
	 *******************************/
	Account();


	Account(string accntName,			//IN - Owner's name
			unsigned short accntNumber,	//IN - Account Number
			Date openDate,				//IN - Opening date
			float balanceAmt,			//IN - Account Balance
			string  acctType);			//IN - Type of account

	virtual ~Account();

	/***************
	 ** MUTATORS  **
	 ***************/

	//this method adds money to the zccount
	virtual void Deposit(float amountIn, Date newDate);

	//this method withdraws from the account if the consditions allow it
	virtual bool Withdraw(float withdrawAmt,
						  Date lastAccesed);

	//updates the account's date
	virtual void UpdateAccount(Date lastOpenDate);

	//initializes the class values
	void SetinitialValues(string accntName,
							  unsigned short accntNumber,
							  Date openDate,
							  float balanceAmt,
							  string  acctType);

	//sets a new account balance
	virtual void  SetBalance(float newBalance);

	//change the account date for a newer one
	void ChangeDates(Date newDate);

	/****************
	 ** ACCESSORS  **
	 ****************/

	//gets the account name
	string GetName() const;

	//gets the account ID
	unsigned short GetAccountID()const;

	//gets the account Balance
	virtual float GetBalance()const;

	//display the info in the account
	string Display(char transType, unsigned short fromNum,
				   float balTransfered, char accntType, Date actionDate, float fromBalance)const;

	//gets the opeining date
	Date GetOpeningDate() const;

private:

	string 			name;			//IN - account owner name
	unsigned short  accountNumber;	//IN - account number
	Date 			openingDate;	//IN - account opening date
	Date            lastAccessDate; //IN - Last time account was accessed
	float 			balance;		//IN - account balance
	string 			accountType;	//IN - account type

};

/***********************************************************************
 * Account Class
 *   This class represents an Account object
 *   It manages 5 attributes:  name, account number, opening date,balance,
 *   and account type.
 ***********************************************************************/

/*******************************
 ** CONSTRUCTOR & DESTRUCTORS **
 *******************************/

/***********************************************************************
 ** Account();
 **   Constructor; Initializes class attributes
 **   Parameter: None
 **   Returns: Nothing
 ***********************************************************************/

/***********************************************************************
 ** Account();
 **   Constructor; Initializes class attributes to the specified values
 **   Parameter: accntName, accntNumber, openDate, accntBalance, accntType
 **   Returns: Nothing
 ***********************************************************************/

/***********************************************************************
 ** ~Account();
 **   Destructor; Does not perform any specific function
 **   Parameter: None
 **   Returns: Nothing
 ***********************************************************************/

/***************
 ** MUTATORS  **
 ***************/

/***********************************************************************
 ** Deposit
 **   Mutator; This function changes the balance in the account (increases)
 **---------------------------------------------------------------------
 **   Parameter: amountIn
 **---------------------------------------------------------------------
 **   Returns: Nothing
 ***********************************************************************/

/***********************************************************************
 ** Withdraw
 **   Mutator; This function decreases the balance, depending upon the amount
 **   of funds.
 **---------------------------------------------------------------------
 **   Parameter: withdrawAmt, accntNumber, and lastAccessed
 **---------------------------------------------------------------------
 **   Returns: bool if found
 ***********************************************************************/

/***********************************************************************
 ** UpdateAccount
 **   Mutator; This function updates all the information in the
 **   account
 **---------------------------------------------------------------------
 **   Parameter: none
 **---------------------------------------------------------------------
 **   Returns: nothing
 ***********************************************************************/

/***********************************************************************
 ** SetinitialValues
 **   Mutator; Initializes class attributes to the specified values
 **---------------------------------------------------------------------
 **   Parameter:accntName, accntNumber, openDate, accntBalance, accntType
 **---------------------------------------------------------------------
 **   Returns: nothing, initializes all the values.
 ***********************************************************************/

/***********************************************************************
 ** SetBalance
 **   Mutator; This function sets a new balance in the account
 **---------------------------------------------------------------------
 **   Parameter: newBalance
 **---------------------------------------------------------------------
 **   Returns: Nothing
 ***********************************************************************/

/***************
 ** ACCESSORS **
 ***************/

/***********************************************************************
 ** GetName
 **   Mutator; gets the account owner name
 **---------------------------------------------------------------------
 **   Parameter: none
 **---------------------------------------------------------------------
 **   Returns: the account owner's name
 ***********************************************************************/

/***********************************************************************
 ** GetAccountID
 **   Mutator; gets the account number
 **---------------------------------------------------------------------
 **   Parameter: none
 **---------------------------------------------------------------------
 **   Returns: the account number
 ***********************************************************************/

/***********************************************************************
 ** GetBalance
 **   Mutator; gets the account balance
 **---------------------------------------------------------------------
 **   Parameter: none
 **---------------------------------------------------------------------
 **   Returns: the account balance
 ***********************************************************************/

/***********************************************************************
 ** GetOpeningDate
 **   Mutator; gets the date in the account
 **---------------------------------------------------------------------
 **   Parameter: none
 **---------------------------------------------------------------------
 **   Returns: current date
 ***********************************************************************/

#endif /* ACCOUNT_H_ */
