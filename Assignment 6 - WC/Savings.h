/*****************************************************
 *AUTHOR         :  The Oz
 *CLASS          :  CS 1B
 *SECTION        :  TTh:  8am - 11:50am
 *DUE DATE       :  3/19/15
 *****************************************************/

#ifndef SAVINGS_H_
#define SAVINGS_H_

#include "Account.h"

class Savings : public Account
{

public:
	/*******************************
	 ** CONSTRUCTOR & DESTRUCTORS **
	 *******************************/
		Savings();

		//non default constructor
		Savings(string accntName,
			unsigned short accntNumber,
			Date openDate,
			float balanceAmt,
			string  acctType);

		//destructor
		virtual ~Savings();

		/***************
		 ** MUTATORS  **
		 ***************/

		//withdraws money from the account
		virtual bool  Withdraw(float withdrawAmt,
							   Date lastAccesed);

		//updates the account information
		virtual void UpdateAccount(Date lastOpenDate);

		//deposits into the account
		virtual void Deposit(float amountIn, Date newDate);

		/****************
		 ** ACCESSORS  **
		 ****************/

		//gets account balance
		virtual float GetBalance() const;

	private:

		//checks if under the limit
		bool IsOverdrawn() const;

		//checks if negative balance
		bool IsNegative() const;

		float interestRate; //IN - monthly interest rate
};

/***********************************************************************
 * Savings Class
 *   This class represents a Savings account object
 *   It manages 0 attributes.
 ***********************************************************************/

/*******************************
 ** CONSTRUCTOR & DESTRUCTORS **
 *******************************/

/***********************************************************************
 ** Savings();
 **   Constructor; Initializes class attributes
 **   Parameter: None
 **   Returns: Nothing
 ***********************************************************************/

/***********************************************************************
 ** ~Savings();
 **   Destructor; Does not perform any specific function
 **   Parameter: None
 **   Returns: Nothing
 ***********************************************************************/

/***************
 ** MUTATORS  **
 ***************/

/***********************************************************************
 ** Withdraw
 **   Mutator; This function decreases the balance, depending upon the amount
 **   of funds.
 **---------------------------------------------------------------------
 **   Parameter: withdrawAmt, accntNumber, and lastAccessed
 **---------------------------------------------------------------------
 **   Returns: bool if found
 ***********************************************************************/

/***********************************************************************
 ** UpdateAccount
 **   Mutator; This function updates all the information in the
 **   account
 **---------------------------------------------------------------------
 **   Parameter: none
 **---------------------------------------------------------------------
 **   Returns: nothing
 ***********************************************************************/

/***********************************************************************
 ** Deposit
 **   Mutator; This function changes the balance in the account (increases)
 **---------------------------------------------------------------------
 **   Parameter: amountIn
 **---------------------------------------------------------------------
 **   Returns: Nothing
 ***********************************************************************/

/***************
** ACCESSORS **
 ***************/

/***********************************************************************
 ** IsOverdrawn
 **   Mutator; gets true or false if the account is under $-200
 **---------------------------------------------------------------------
 **   Parameter: none
 **---------------------------------------------------------------------
 **   Returns: true or false
 ***********************************************************************/

/***********************************************************************
 ** IsNegative
 **   Mutator; gets true or false if the account is under $0
 **---------------------------------------------------------------------
 **   Parameter: none
 **---------------------------------------------------------------------
 **   Returns: true or false
 ***********************************************************************/

/***********************************************************************
 ** GetBalance
 **   Mutator; gets the account balance
 **---------------------------------------------------------------------
 **   Parameter: none
 **---------------------------------------------------------------------
 **   Returns: the account balance
 ***********************************************************************/


#endif /* SAVINGS_H_ */
