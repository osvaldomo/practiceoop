/*****************************************************
 *AUTHOR         :  The Oz
 *CLASS          :  CS 1B
 *SECTION        :  TTh:  8am - 11:50am
 *DUE DATE       :  5/14/15
 *****************************************************/

#include "Account.h"

/**********************************************************************
 * Account :: Account
 *
 *	This Constructor Initializes class attributes specified in the class
 *	declaration
 *
 *	PRECONDITIONS
 *		NONE
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: nothing - initializes values
 *********************************************************************/
Account :: Account()
{
	name.clear();
	accountNumber = 0;
	openingDate.SetDate(0,0,0);
	balance 	  = 0.0;
	accountType.clear();
}

/**********************************************************************
 * Account :: Account(accntName, accntNumber, openDate, balanceAmt, acctType)
 *
 *	This Constructor Initializes class attributes specified in the Account
 *	class.
 *
 *	PRECONDITIONS
 *		accntName    :  Owner's name
 *		accntNumber  :  Account Number
 *		openDate     :  Date the account was created
 *		balanceAmt	 :  Current balance
 *		acctType     :  type of account
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: nothing - initializes all values
 *********************************************************************/
Account :: Account(string accntName,			//IN - Owner's name
					unsigned short accntNumber,	//IN - Account Number
					Date openDate,				//IN - Date the account was created
					float balanceAmt,			//IN - Current balance
					string  acctType)			//IN - type of account
{
	name 			= accntName;
	accountNumber 	= accntNumber;

	openingDate 	= openDate;

	balance 		= balanceAmt;

	accountType 	= acctType;
}

/**********************************************************************
 * Account :: ~Account
 *
 *	This Destructor ends the class
 *
 *	PRECONDITIONS
 *		NONE
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: nothing
 *********************************************************************/
Account :: ~Account()
{

}

/**********************************************************************
 * Account :: SetinitialValues(accntName, accntNumber, openDate,
 * 							   balanceAmt, acctType)
 *
 *	This Constructor Initializes class attributes specified in the Account
 *	class.
 *
 *	PRECONDITIONS
 *		accntName    :  Owner's name
 *		accntNumber  :  Account Number
 *		openDate     :  Date the account was created
 *		balanceAmt	 :  Current balance
 *		acctType     :  type of account
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: nothing - initializes all values
 *********************************************************************/
void Account :: SetinitialValues(string accntName,			//IN - Owner's name
								unsigned short accntNumber,	//IN - Account Number
								Date openDate,				//IN - Date the
															//account was created
								float balanceAmt,			//IN - Current balance
								string  acctType)			//IN - type of account
{
	name 			= accntName;
	accountNumber 	= accntNumber;

	openingDate 	= openDate;

	lastAccessDate 	= openDate;

	balance 		= balanceAmt;

	accountType 	= acctType;

}

/**********************************************************************
 * Account :: UpdateAccount(lastOpenDate)
 *
 *	This Method updates the information in the account. It sets a
 *	new date and decreases or decreases the account balance.
 *
 *	PRECONDITIONS
 *
 *		lastOpenDate     :  Date the account was created
 *
 *	POSTCONDITIONS
 *		Updated account
 *
 * RETURNS: nothing - updates account
 *********************************************************************/
void Account :: UpdateAccount(Date lastOpenDate) //IN - Current opening date
{

}

/**********************************************************************
 * Account :: Withdraw(withdrawAmt lastAccesed)
 *
 *	This Method Withdrwas money from an account, if the account is overdrawn,
 *	then, there is nothing to retrieve.
 *
 *	PRECONDITIONS
 *		lastAccesed  :  The day the account was last accessed
 *		accntNumber  :  Account Number
 *		withdrawAmt	 :  Amount to withdraw
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: true or false
 *********************************************************************/
bool Account :: Withdraw(float withdrawAmt,	// IN - Withdraw amount
						 Date lastAccesed)	// IN - The day the account
											//		was last accessed
 {

	cout << endl << "usando este metodo" << endl;

	 return false;

 }

 /**********************************************************************
  * Account :: SetBalance
  *
  *	This method sets a new balance into the account
  *
  *	PRECONDITIONS
  *		newBalance : The new account balance
  *
  *	POSTCONDITIONS
  *		NONE
  *
  * RETURNS: nothing - sets new balance
  *********************************************************************/
 void  Account :: SetBalance(float newBalance) //IN - The new account balanceS
 {

 	balance = newBalance;

 }

 /**********************************************************************
   * Account :: Deposit
   *
   *	This method adds money to the accounts and updates the
   *	information as well
   *
   *	PRECONDITIONS
   *		amountIn : Amount to be added
   *		newDate	 : New opening date
   *
   *	POSTCONDITIONS
   *		NONE
   *
   * RETURNS: nothing - sets adds balance
   *********************************************************************/
 void Account :: Deposit(float amountIn, //IN - Amount to add to the account
		 	 	 	 	 Date newDate)   //IN - New opening date
 {

	 UpdateAccount(newDate);
	 SetBalance(GetBalance() + amountIn);

 }

 /**********************************************************************
   * Account :: ChangeDates
   *
   *	This method sets a new date into the account
   *
   *	PRECONDITIONS
   *		newDate : The new account balance
   *
   *	POSTCONDITIONS
   *		NONE
   *
   * RETURNS: nothing - sets new date
   *********************************************************************/
 void Account :: ChangeDates(Date newDate)
 {
	  openingDate 	 = newDate;

 }
