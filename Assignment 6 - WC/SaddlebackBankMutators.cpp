/*****************************************************
 *AUTHOR         :  The Oz
 *CLASS          :  CS 1B
 *SECTION        :  TTh:  8am - 11:50am
 *DUE DATE       :  5/14/15
 *****************************************************/

#include "SaddlebackBank.h"

/**********************************************************************
 * Bank :: Bank
 *
 *	This Constructor Initializes class attributes specified in the class
 *	declaration
 *
 *	PRECONDITIONS
 *		NONE
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: nothing - initializes values
 *********************************************************************/
Bank :: Bank()
{
	head = NULL;
	tail = NULL;
}

/**********************************************************************
 * Account :: ~Account
 *
 *	This Destructor ends the class
 *
 *	PRECONDITIONS
 *		NONE
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: nothing
 *********************************************************************/
Bank :: ~Bank()
{

	BankNode   *deleteAcct;
	deleteAcct = head;

	while(head != NULL)
	{
		head = head->next;
		delete deleteAcct;
		deleteAcct = head;
	}

	deleteAcct = NULL;
}

/**********************************************************************
  * Bank :: Deposit(newBalance)
  *
  *	This Method deposits to the account, increments the amount of
  *	money in the account.
  *
  *	PRECONDITIONS
  *		newBalance	 :  Amount to add
  *
  *	POSTCONDITIONS
  *		increments balance
  *
  * RETURNS: nothing
  *********************************************************************/
void Bank :: Deposit(Account destination,
					 float depositAmt)
{

}

/**********************************************************************
  * Bank :: Transfer(amountTransfer, originalAcct, destinyAcct,
  * 				actionDate, oFile)
  *
  *	This Method deposits to the account, increments the amount of
  *	money in the account.
  *
  *	PRECONDITIONS
  *		amountTransfer	 :  Amount to transfer
  *		originalAcct	 :	where the money comes from
  *		destinyAcct		 :	where the money is going
  *		actionDate		 :	date of the transaction
  *		oFile			 :	output stream
  *
  *	POSTCONDITIONS
  *		NONE
  *
  * RETURNS: nothing
  *********************************************************************/
void Bank :: Transfer(float  amountTransfer,		//IN - Amount to transfer
			  	  	  unsigned short originalAcct,	//IN - where the money comes from
					  unsigned short destinyAcct,	//IN - where the money is going
					  Date actionDate,				//IN - date of the transaction
					  ofstream &oFile)				//IN - output stream
{
	Account *accnt1;	//CALC/OUT - original account pointer
	Account *accnt2;	//CALC/OUT - second account pointer


	accnt2 = FindAccount(originalAcct);

	accnt1 = FindAccount(destinyAcct);

if(accnt1 != NULL && accnt2 != NULL)
{

	if(accnt1->GetAccountID() != accnt2->GetAccountID())
	{
			if(accnt1->Withdraw(amountTransfer, actionDate))
			{
			accnt2->Deposit(amountTransfer, actionDate);

			oFile << accnt2 ->Display('T', destinyAcct, amountTransfer, ' '
										, actionDate,accnt1->GetBalance());
			}//true withdraw

			else
			{
			oFile << endl << "*** TRANSFER FROM " << accnt1->GetAccountID()
				  <<" TO " << accnt2->GetAccountID() <<" NOT ALLOWED! ***"
				  << endl << "***      DUE TO INSUFFICIENT FUNDS          ***"
				  << endl << endl;
			}//not withdraw

	}//if account not the same
	else
	{

		oFile << endl << "*** ERROR - CAN'T TRANSFER INTO THE SAME ACCOUNT ***"
			  << endl << endl;

	}//else is the same accont

}//if not null
else
{
	oFile << endl << "*** ERROR - ACCOUNT NOT FOUND ***" << endl << endl;
}//else is null

}

/**********************************************************************
  * Bank :: OpenAccount(newAccount)
  *
  *	This Method adds a new pointer to the list of accounts
  *
  *	PRECONDITIONS
  *		newAccount	 :  account to add to the list
  *
  *	POSTCONDITIONS
  *		NONE
  *
  * RETURNS: nothing
  *********************************************************************/
void Bank :: OpenAccount(Account *newAccount)//IN - account to add to the list
{

			bool        found;			//PROC - true if the item is found
			BankNode   *searchPtr;	    //PROC - Pointer to move through the list
			BankNode   *bankNodePtr;    //PROC - pointer to add

			searchPtr   = new BankNode;
			searchPtr   = head;
			bankNodePtr = new BankNode;

			if(bankNodePtr != NULL)
			{
				//PROC - Assign the new sheep object to the sheep node pointer to add
				bankNodePtr->myAccountNode = newAccount;

				//PROC - Case if list is empty or new node needs to be in the front
				if(IsEmpty() ||
				   head-> myAccountNode->GetAccountID() > bankNodePtr->myAccountNode->GetAccountID())
				{

					bankNodePtr->prev = NULL;
					bankNodePtr -> next = head;
					if(!IsEmpty())
					{
						head->prev = bankNodePtr;
					}
					head = bankNodePtr;
					bankNodePtr = NULL;
				}
				//PROC - Find the proper location
				else
				{

					while(searchPtr->next != NULL && !found)
					{

						if(searchPtr->next->myAccountNode->GetAccountID() >
						bankNodePtr->myAccountNode->GetAccountID())
						{
							found = true;
						}
						else
						{
							searchPtr = searchPtr->next;
						}
					}
					//PROC - Insert the new node in the found location
					bankNodePtr -> prev = searchPtr;
					bankNodePtr -> next = searchPtr -> next;
					searchPtr     -> next = bankNodePtr;

					//PROC - Link in the node to the tail end of list unless at end


					if(bankNodePtr -> next != NULL)
					{
						bankNodePtr -> next -> prev = bankNodePtr;
						tail = bankNodePtr;
					}
					bankNodePtr = NULL;
				}

			}
			else
			{
				cout << "**** OUT OF MEMORY!!****";
			}

			delete bankNodePtr;
			bankNodePtr = NULL;
	}
