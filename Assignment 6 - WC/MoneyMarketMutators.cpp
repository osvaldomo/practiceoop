/*****************************************************
 *AUTHOR         :  The Oz
 *CLASS          :  CS 1B
 *SECTION        :  TTh:  8am - 11:50am
 *DUE DATE       :  5/14/15
 *****************************************************/

#include "MoneyMarket.h"

/**********************************************************************
 * MoneyMarket :: MoneyMarket
 *
 *	This Constructor Initializes class attributes specified in the class
 *	declaration
 *
 *	PRECONDITIONS
 *		NONE
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: nothing - initializes values
 *********************************************************************/
MoneyMarket :: MoneyMarket()
{
	 interestRate   =  .20;

	 withdrawalFee  = 1.5;
}

/**********************************************************************
 * MoneyMarket :: MoneyMarket(intRate,fee)
 *
 *	This Constructor Initializes class attributes specified in the class
 *	declaration
 *
 *	PRECONDITIONS
 *		intRate : interest rate
 *		fee     : withdrawal fee
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: nothing - initializes values
 *********************************************************************/
MoneyMarket :: MoneyMarket(string accntName,
							unsigned short accntNumber,
							Date openDate,
							float balanceAmt,
							string  acctType)  //IN - withdrawal fee
		:Savings(accntName, accntNumber, openDate, balanceAmt, acctType)
{

	interestRate   =  .20;

	withdrawalFee  = 1.5;

}

/**********************************************************************
 * MoneyMarket :: ~MoneyMarket
 *
 *	This Destructor ends the class
 *
 *	PRECONDITIONS
 *		NONE
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: nothing
 *********************************************************************/
MoneyMarket :: ~MoneyMarket()
{

}

/**********************************************************************
 * MoneyMarket :: Withdraw(withdrawAmt, accntNumber, lastAccesed)
 *
 *	This Method Withdrwas money from an account, if the account is overdrawn,
 *	then, there is nothing to retrieve.
 *
 *	PRECONDITIONS
 *		lastAccesed  :  The day the account was last accessed
 *		accntNumber  :  Account Number
 *		withdrawAmt	 :  Amount to withdraw
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: true or false
 *********************************************************************/
 bool MoneyMarket :: Withdraw(float withdrawAmt,  // IN - amount to withdraw
		  	  	  	  	  	  Date lastAccesed)	  // IN - last accessed date
 {

	 bool withdraw = false;

	 UpdateAccount(lastAccesed);

	 if(!IsEmpty() && withdrawAmt < GetBalance())
	 {
		 SetBalance(GetBalance() - withdrawAmt - withdrawalFee);

		 withdraw = true;

	 }
	 else if(!IsEmpty() && withdrawAmt > GetBalance())
	 {

		 withdraw = false;
	 }

	 return withdraw;
 }

 /**********************************************************************
   * MoneyMarket :: UpdateAccount(lastOpenDate)
   *
   *	This Method updates the information in the account. It sets a
   *	new date and decreases or decreases the account balance.
   *
   *	PRECONDITIONS
   *
   *		LastOpenDate     :  Date the account was created
   *
   *	POSTCONDITIONS
   *		Updated account
   *
   * RETURNS: nothing - updates account
   *********************************************************************/
 void MoneyMarket :: UpdateAccount(Date lastOpenDate)
 {

	 int months;  //difference in months
	 int index;	  //for loop lvc

	 	 months = lastOpenDate.GetMonth() - GetOpeningDate().GetMonth();

	 	 for(index = 0; index < months; index ++)
	 	 {
	 		 if(GetBalance() != 0)
	 		 {

	 			 SetBalance(GetBalance() + GetBalance() * interestRate );
	 		 }
	 	 }

	 	 ChangeDates(lastOpenDate);
 }

 /**********************************************************************
   * MoneyMarket :: Deposit(amountIn, newDate)
   *
   *	This Method deposits to the account, increments the amount of
   *	money in the account.
   *
   *	PRECONDITIONS
   *		newBalance	 :  Amount to add
   *		newDate		 :  New date to update
   *
   *	POSTCONDITIONS
   *		increments balance
   *
   * RETURNS: nothing
   *********************************************************************/
 void MoneyMarket :: Deposit(float amountIn,// amount to deposit
		 	 	 	 	 	 Date newDate)  // new opening date
 {
	 UpdateAccount(newDate);
	 SetBalance(GetBalance() + amountIn);
 }
