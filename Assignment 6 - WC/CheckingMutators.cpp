/*****************************************************
 *AUTHOR         :  The Oz
 *CLASS          :  CS 1B
 *SECTION        :  TTh:  8am - 11:50am
 *DUE DATE       :  5/14/15
 *****************************************************/

#include "Checking.h"

/**********************************************************************
 * Checking :: Checking
 *
 *	This Constructor Initializes class attributes specified in the class
 *	declaration
 *
 *	PRECONDITIONS
 *		NONE
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: nothing - initializes values
 *********************************************************************/
Checking :: Checking()
{

}

Checking :: Checking(string accntName,           //IN - owner's name
					unsigned short accntNumber,  //IN - Account Number
					Date openDate,				 //IN - Opening date
					float balanceAmt,			 //IN - Account balance
					string  acctType): Account(accntName, accntNumber,
								   openDate, balanceAmt, acctType)
{


}

/**********************************************************************
 * Checking :: ~Checking
 *
 *	This Destructor ends the class
 *
 *	PRECONDITIONS
 *		NONE
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: nothing
 *********************************************************************/
Checking :: ~Checking()
{

}

/**********************************************************************
 * Checking :: Withdraw(withdrawAmt, accntNumber, lastAccesed)
 *
 *	This Method Withdrwas money from an account, if the account is overdrawn,
 *	then, there is nothing to retrieve.
 *
 *	PRECONDITIONS
 *		lastAccesed  :  The day the account was last accessed
 *		withdrawAmt	 :  Amount to withdraw
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: true or false
 *********************************************************************/
 bool Checking :: Withdraw(float withdrawAmt,  // IN - Amount to withdraw
		  	  	  	  	   Date lastAccesed)   // IN - The day the account
											  //		was last accessed
 {

	 bool withdraw = false;

	 UpdateAccount(lastAccesed);

	 if(!IsNegative() && withdrawAmt < GetBalance())
	 {
		 SetBalance(GetBalance() - withdrawAmt);

		 withdraw = true;

	 }
	 else if(!IsNegative() && withdrawAmt > GetBalance() && !IsOverdrawn())
	 {
		 SetBalance(GetBalance() - withdrawAmt - 20.0);

		 UpdateAccount(lastAccesed);

		 withdraw = true;
	 }

	 return withdraw;
 }

 /**********************************************************************
  * Checking :: UpdateAccount(lastOpenDate)
  *
  *	This Method updates the information in the account. It sets a
  *	new date and decreases or decreases the account balance.
  *
  *	PRECONDITIONS
  *
  *		LastOpenDate     :  Date the account was created
  *
  *	POSTCONDITIONS
  *		Updated account
  *
  * RETURNS: nothing - updates account
  *********************************************************************/
 void Checking :: UpdateAccount(Date lastOpenDate) //Last day the account was open
 {

	 int months;
	 int index;

	 months = lastOpenDate.GetMonth() - GetOpeningDate().GetMonth();

	 for(index = 0; index < months; index ++)
	 {
		 if(GetBalance() < 0)
		 {

			 SetBalance(GetBalance() - 20.0);
		 }
	 }

	 ChangeDates(lastOpenDate);

 }

/**********************************************************************
  * Checking :: Deposit(amountIn, newDate)
  *
  *	This Method deposits to the account, increments the amount of
  *	money in the account.
  *
  *	PRECONDITIONS
  *		newBalance	 :  Amount to add
  *		newDate		 :  New date to update
  *
  *	POSTCONDITIONS
  *		increments balance
  *
  * RETURNS: nothing
  *********************************************************************/
void  Checking :: Deposit(float amountIn, 	//IN - Amount to add
						  Date newDate) 	//IN - New date to update
{
		UpdateAccount(newDate);
		SetBalance(GetBalance() + amountIn);
}
