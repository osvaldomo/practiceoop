/*****************************************************
 *AUTHOR         :  The Oz
 *CLASS          :  CS 1B
 *SECTION        :  TTh:  8am - 11:50am
 *DUE DATE       :  5/14/15
 *****************************************************/

#include "myHeader.h"
#include "Account.h"
#include "Savings.h"

/************************************************************************
 * Saddleback Bank - OOP
 * ______________________________________________________________________
 * This program will use object oriented programming to represent banking
 * accounts and manage banking activities. The program will be develop a
 * set of classes to simulate various types of bank accounts and demonstrate
 * the principles of inheritance. First, the Account class is the base
 * class for all accounts, which has the following attributes: account
 * owner’s name, account number, account opening date and account balance.
 * The Account class has two derived classes, Checking and Savings accounts.
 * The main program will use the class defined above and process a
 * number of account transactions from an input file. Each transaction
 * will include the account number, transaction date and amount.
 * Main will output all transactions and account balances in an output file
 * ______________________________________________________________________
 * INPUT:
 *   fileName    :  Input file Name
 *   outFileName :  output file name
 *
 * Output:
 * 	oFile         : output stream
 *
 * CONSTANTS
 *	NUM     : project number
 *	NAME    : Programmer's name
 *	TYPE    : Assignment ('A') or Lab ('L')
 *
 *	OPTION_MAX : Maximum option number the user can input.
 *	OPTION_MIN : Minimum option number the user can input.
 ************************************************************************/

int main()
{

	ofstream oFile; //OUT - output stream
	ifstream inFile;//IN  - Input file stream

	Date 			actionDate;     //IN - Updated date
	unsigned short 	comingFrom;		//IN - First account ID
	float 			amntTransfer;	//IN - Amount to transfer
	string 			action;			//IN - Type of transfer to take
	unsigned short 	destinationAccnt;//IN - where is the money going
	bool 			valid;			 //IN - if the data is valid

	const string ACCOUNTS_FILE = "Accounts.txt";
	const string TRANSACTIONS_FILE = "Transactions.txt";

	const string OFILE = "outputFile.txt";

	Bank 	myBank;		//CALC & OUT - The accounts list
	Account *myAccount; //CALC       - Account pointer

	myAccount = new Account;

	//Open file
	oFile.open(OFILE.c_str());
	inFile.open(TRANSACTIONS_FILE.c_str());

	//OUTPUT - Outputs class header to an output file.
	PrintHeader(oFile, "Saddleback Bank - OOP", "The Oz", 'A', 6);

	//display header
	oFile << left;
	oFile << setw(18) << "TRANSACTION" << setw(13) << "DATE" << setw(8)
		  << "ACCT #" << setw(24) << "ACCT NAME" << right
		  << "AMOUNT" << setw(17) << "BALANCE"  << setw(15)
		  << "FROM ACCT #"<< setw(16) << "FROM ACCT BAL" << endl;

	oFile << left;
	oFile << setw(18) << Fill( '-', 12) << setw(13) << Fill( '-', 10) << setw(8) <<
			Fill( '-', 5) << setw(24) << Fill('-', 20) << setw(14) <<
			Fill('-', 10) << setw(11) << Fill('-', 9)  << left << setw(14) <<
			Fill('-', 13) << setw(13) << Fill('-', 15) << endl;

		//read the data from the file
		ReadInfoFromFile(myBank, ACCOUNTS_FILE, oFile);

		//read the instructions from the file
		while(inFile)
		{

		//Processing - get all the instructions and return them by reference
		ReadInstructionsFromFile(actionDate, comingFrom, amntTransfer, action,
							 destinationAccnt,inFile );
		if(action == "Deposit")
		{

			myAccount = myBank.FindAccount(comingFrom);
			myAccount->Deposit(amntTransfer, actionDate);

			oFile << myAccount ->Display('D', 0, amntTransfer, ' ', actionDate, 0.0);
		}
		else if(action == "Withdrawal")
		{

			myAccount = myBank.FindAccount(comingFrom);

			valid = myAccount->Withdraw(amntTransfer, actionDate);

			if(valid)
			{
				oFile << myAccount ->Display('W', 0, amntTransfer, ' ', actionDate, 0.0);
			}
			else
			{
				oFile << endl << "*** ERROR - CAN'T WITHDRAW DUE TO INSUFFICIENT FUNDS ***" << endl;
			}

		}
		else if(action == "Transfer")
		{

			myBank.Transfer(amntTransfer,comingFrom,destinationAccnt,actionDate, oFile);
		}//end of while loop

		}
		inFile.close();

		oFile.close();

	return 0;
}
