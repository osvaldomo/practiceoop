/*****************************************************
 *AUTHOR         :  The Oz
 *CLASS          :  CS 1B
 *SECTION        :  TTh:  8am - 11:50am
 *DUE DATE       :  5/14/15
 *****************************************************/

#include "Savings.h"

/**********************************************************************
 * Savings :: Savings
 *
 *	This Constructor Initializes class attributes specified in the class
 *	declaration
 *
 *	PRECONDITIONS
 *		NONE
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: nothing - initializes values
 *********************************************************************/
Savings :: Savings()
{
	interestRate = .10;
}

Savings :: Savings(string accntName,		//IN - owner's name
				unsigned short accntNumber, //IN - account number
				Date openDate,				//IN - opening date
				float balanceAmt,			//IN - new balance
				string  acctType):Account(accntName, accntNumber,
										  openDate, balanceAmt, acctType)
{

	interestRate = .10;

}

/**********************************************************************
 * Savings :: ~Savings
 *
 *	This Destructor ends the class
 *
 *	PRECONDITIONS
 *		NONE
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: nothing
 *********************************************************************/
Savings :: ~Savings()
{

}

/**********************************************************************
 * Withdraw :: Withdraw(withdrawAmt, accntNumber, lastAccesed)
 *
 *	This Method Withdrwas money from an account, if the account is overdrawn,
 *	then, there is nothing to retrieve.
 *
 *	PRECONDITIONS
 *		lastAccesed  :  The day the account was last accessed
 *		withdrawAmt	 :  Amount to withdraw
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: true or false
 *********************************************************************/
 bool Savings :: Withdraw(float withdrawAmt,//IN - amount to withdraw
		 	 	 	      Date lastAccesed)// IN - The day the account
 {

	 bool withdraw = false;

	 UpdateAccount(lastAccesed);

	 if(!IsNegative() && withdrawAmt < GetBalance())
	 {

		 SetBalance(GetBalance() - withdrawAmt);

		 withdraw = true;

	 }
	 return withdraw;
 }

 /**********************************************************************
  * Savings :: UpdateAccount(lastOpenDate)
  *
  *	This Method updates the information in the account. It sets a
  *	new date and decreases or decreases the account balance.
  *
  *	PRECONDITIONS
  *
  *		LastOpenDate     :  Date the account was created
  *
  *	POSTCONDITIONS
  *		Updated account
  *
  * RETURNS: nothing - updates account
  *********************************************************************/
 void Savings :: UpdateAccount(Date lastOpenDate) //IN - Date it was open
 {
	  int months;
	  int index;

	 months = lastOpenDate.GetMonth() - GetOpeningDate().GetMonth();

	 for(index = 0; index < months; index ++)
	 {
		 if(GetBalance() != 0)
		 {

			 SetBalance(GetBalance() + GetBalance() * interestRate);

		 }
	 }

	 ChangeDates(lastOpenDate);

 }

 /**********************************************************************
   * Savings :: Deposit(amountIn, newDate)
   *
   *	This Method deposits to the account, increments the amount of
   *	money in the account.
   *
   *	PRECONDITIONS
   *		newBalance	 :  Amount to add
   *		newDate		 :  New date to update
   *
   *	POSTCONDITIONS
   *		increments balance
   *
   * RETURNS: nothing
   *********************************************************************/
void  Savings :: Deposit(float amountIn, //IN - Amount to add
						 Date newDate)	 //IN - new account date
{

	UpdateAccount(newDate);
	SetBalance(GetBalance() + amountIn);
}
