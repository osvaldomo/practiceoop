/*****************************************************
 *AUTHOR         :  The Oz
 *CLASS          :  CS 1B
 *SECTION        :  TTh:  8am - 11:50am
 *DUE DATE       :  5/14/15
 *****************************************************/

#include "Account.h"

/**********************************************************************
 * Account :: GetAccountID
 *
 *	This method returns teh account ID.
 *
 *	PRECONDITIONS
 *		NONE
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: the account ID as a unsigned short
 *********************************************************************/
unsigned short Account :: GetAccountID()const
{

	return accountNumber;

}

/**********************************************************************
 * Account :: GetName
 *
 *	This method returns the name of the account owner
 *
 *	PRECONDITIONS
 *		NONE
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: the account owner's name
 *********************************************************************/
string Account :: GetName() const
{
	return name;
}

/**********************************************************************
 * Account :: GetBalance
 *
 *	This method returns the current balance in the account
 *
 *	PRECONDITIONS
 *		NONE
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: the account balance
 *********************************************************************/
float Account :: GetBalance()const
{

	return balance;

}

/**********************************************************************
 * Account :: Display
 *
 *	This method displays the information in the account depending upon
 *	the type of actions.
 *
 *	PRECONDITIONS
 *		transType 	  	: type of transaction
 *		fromNum   		: from account
 *		balTransfered 	: Balance transfered
 *		accntType 		: type of account
 *		actionDate		: date of transaction
 *		fromBalance		: Balance from transfered account
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: the account balance
 *********************************************************************/
string Account :: Display(char transType, 		 //IN - type of transaction
						  unsigned short fromNum,//IN - from account
						  float balTransfered,   //IN - Balance transfered
						  char accntType,		 //IN - type of account
						  Date actionDate,		 //IN - date of transaction
						  float fromBalance)const//IN - Balance from
												 //		transfered account
{
	ostringstream out;  //OUT - output String stream

	out << left;

	//PROCESSING - type of account
	switch(transType)
	{

	case 'N' :
				out << "OPEN ";
				if(accntType == 'C')
				{
					out << setw(13) << "CHECKING";
				}
				else if(accntType == 'S')
				{

					out << setw(13) << "SAVINGS";

				}
				else
				{
					out << setw(13) << "Money Market";
				}
				break;

	case 'D' : out << setw(18)<< "Deposit";
	break;

	case 'W' : out << setw(18)<< "Withdrawal";
	break;

	case 'T' : out << setw(18)<< "Transfer";
	break;
	}

	//OUTPUT - data info if it is not a transaction
	out << setprecision(2) << fixed;
	out << setw(13) << actionDate.DisplayDate() << setw(8) << GetAccountID() << setw(22)
		<< GetName() << setw(2) << right << "$" << setw(10)<< balTransfered << setw(5) << right << "$" << setw(8)<< GetBalance();

	//OUTPUT - data info if it is not a transaction
	if(transType == 'T')
	{
		out << left;
		out << setprecision(2) << fixed;
		out <<setw(14) << right << fromNum << setw(6)  << "$" << setw(12) << fromBalance << right << endl;
	}
	else
	{
		out << endl;
	}
	return out.str();

}

/**********************************************************************
 * Account :: GetOpeningDate
 *
 *	This method returns the current date in the account
 *
 *	PRECONDITIONS
 *		NONE
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: the current date
 *********************************************************************/
Date Account :: GetOpeningDate() const
{
	return openingDate;
}





