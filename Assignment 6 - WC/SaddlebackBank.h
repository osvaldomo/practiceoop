/*****************************************************
 *AUTHOR         :  The Oz
 *CLASS          :  CS 1B
 *SECTION        :  TTh:  8am - 11:50am
 *DUE DATE       :  5/12/15
 *****************************************************/

#ifndef SADDLEBACKBANK_H_
#define SADDLEBACKBANK_H_

#include <iostream>
#include <iomanip>
#include <string>
#include <ostream>
#include <fstream>
#include <sstream>
#include "Date.h"

 using namespace std;

#include "Account.h"
#include "Savings.h"
#include "Checking.h"
#include "MoneyMarket.h"

class Bank
{

public :

	 //default constructor
	Bank();

	//destructor
	virtual ~Bank();

	//deposits money into the account
	virtual void    Deposit(Account destination, float depositAmt);

	/***************
	 ** MUTATORS  **
	 ***************/

	//transfers money between accounts
	 void Transfer(float 		 amountTransfer,
						  unsigned short originalAcct,
						  unsigned short destinyAcct,
						  Date actionDate,
						  ofstream &oFile);

	 //creates a new account
	void OpenAccount(Account *newAccount);

	/****************
	 ** ACCESSORS  **
	 ****************/

	//finds an account and returns it
	Account *FindAccount(unsigned short accntNumber) const;

private:

	//checks if the list is empty
	bool IsEmpty() const;

	struct BankNode
	{

		Account  * myAccountNode;
		BankNode * next;
		BankNode * prev;

	};

	BankNode * head;
	BankNode * tail;

};

/***********************************************************************
 * Bank Class
 *   This class represents a Banking list object
 *   It manages 3 attributes. Head, Tail, and BankNode struct
 ***********************************************************************/

/*******************************
 ** CONSTRUCTOR & DESTRUCTORS **
 *******************************/

/***********************************************************************
 ** Bank();
 **   Constructor; Initializes class attributes
 **   Parameter: None
 **   Returns: Nothing
 ***********************************************************************/

/***********************************************************************
 ** ~Bank();
 **   Destructor; Does not perform any specific function
 **   Parameter: None
 **   Returns: Nothing
 ***********************************************************************/

/***************
 ** MUTATORS  **
 ***************/

/***********************************************************************
 ** OpenAccount
 **   Mutator; This function creates a new account pointer in the
 **   list of accounts.
 **---------------------------------------------------------------------
 **   Parameter: newAccount
 **---------------------------------------------------------------------
 **   Returns: nothing, makes a new account
 ***********************************************************************/

/***********************************************************************
 ** Transfer
 **   Mutator; This function transfers money from one account to the other,
 **   if there are the necessary conditions.
 **---------------------------------------------------------------------
 **   Parameter: amountTransfer, originalAcct, destinyAcct
 **---------------------------------------------------------------------
 **   Returns: true or false
 ***********************************************************************/

/***********************************************************************
 ** Deposit
 **   Mutator; This function changes the balance in the account (increases)
 **---------------------------------------------------------------------
 **   Parameter: destination, depositAmt
 **---------------------------------------------------------------------
 **   Returns: Nothing
 ***********************************************************************/

/***************
** ACCESSORS **
 ***************/

/***********************************************************************
 ** IsEmpty
 **   Mutator; gets true or false if the account list empty
 **---------------------------------------------------------------------
 **   Parameter: none
 **---------------------------------------------------------------------
 **   Returns: true or false
 ***********************************************************************/

/***********************************************************************
 ** FindAccount
 **   Mutator; gets an account and returns it.
 **---------------------------------------------------------------------
 **   Parameter: accntNumber
 **---------------------------------------------------------------------
 **   Returns: The account pointer
 ***********************************************************************/

#endif /* SADDLEBACKBANK_H_ */
