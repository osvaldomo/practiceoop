/*****************************************************
 *AUTHOR         :  The Oz
 *CLASS          :  CS 1B
 *SECTION        :  TTh:  8am - 11:50am
 *DUE DATE       :  5/14/15
 *****************************************************/

#include "SaddlebackBank.h"

/**********************************************************************
 * Bank :: IsEmpty
 *
 *	This method returns if the List has no Accounts
 *
 *	PRECONDITIONS
 *		NONE
 *	POSTCONDITIONS
 *		true of false
 * RETURNS: true of false
 *********************************************************************/
bool Bank :: IsEmpty() const
{
	return head == NULL;
}

/**********************************************************************
 * Bank :: FindAccount
 *
 *	This method returns if the List has no Accounts
 *
 *	PRECONDITIONS
 *		findAccount : Account to find
 *
 *	POSTCONDITIONS
 *		NONE
 *
 * RETURNS: pointer by reference
 *********************************************************************/
Account* Bank :: FindAccount(unsigned short accntNumber) const//IN - Account to find
{
	bool       found;      //CALC - if found
	BankNode *currentNode; //CALC - looping pointer
	Account  *foundAccount;//CALC/OUT - Stores found pointer

	foundAccount = NULL;

	found       = false;
	currentNode = head;

	if(!IsEmpty())
	{
		while(currentNode != NULL && !found)
		{
			if(currentNode->myAccountNode->GetAccountID() != accntNumber)
			{
				currentNode = currentNode->next;
			}
			else
			{
				found = true;
				foundAccount = currentNode->myAccountNode;
			}
		}
}

	return foundAccount;
}
