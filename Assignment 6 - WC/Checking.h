/*****************************************************
 *AUTHOR         :  The Oz
 *CLASS          :  CS 1B
 *SECTION        :  TTh:  8am - 11:50am
 *DUE DATE       :  5/14/15
 *****************************************************/

#ifndef CHECKING_H_
#define CHECKING_H_

#include "Account.h"

class Checking : public Account
{
public:

	/*******************************
	 ** CONSTRUCTOR & DESTRUCTORS **
	 *******************************/

	//default constructor
	Checking();

	//nondefault constructor
	Checking(string accntName,
			unsigned short accntNumber,
			Date openDate,
			float balanceAmt,
			string  acctType);

	//destructor
	virtual ~Checking();

	/***************
	 ** MUTATORS  **
	 ***************/

	//withdraw amount
	virtual bool Withdraw(float withdrawAmt,
			 			  Date lastAccesed);

	//update the account
	virtual void UpdateAccount(Date lastOpenDate);

private:

	//deposits to the account
	virtual void Deposit(float amountIn, Date newDate);

	/****************
	 ** ACCESSORS  **
	 ****************/

	//checks if account is under -$200
	bool IsOverdrawn() const;

	//if the balance is negative
	bool IsNegative() const;

	//gets the account balance
	virtual float GetBalance() const;

};

/***********************************************************************
 * Checking Class
 *   This class represents a Checking account object
 *   It manages 0 attributes.
 ***********************************************************************/

/*******************************
 ** CONSTRUCTOR & DESTRUCTORS **
 *******************************/

/***********************************************************************
 ** Checking();
 **   Constructor; Initializes class attributes
 **   Parameter: None
 **   Returns: Nothing
 ***********************************************************************/

/***********************************************************************
 ** Checking(accntName,
			 accntNumber,
			 openDate,
			 balanceAmt,
			 acctType)

 **   Constructor; Initializes class attributes
 **   Parameter: accntName, accntNumber, openDate, balanceAmt, acctType
 **   Returns  : Nothing
 ***********************************************************************/

/***********************************************************************
 ** ~Checking();
 **   Destructor; Does not perform any specific function
 **   Parameter: None
 **   Returns: Nothing
 ***********************************************************************/

/***************
 ** MUTATORS  **
 ***************/

/***********************************************************************
 ** Withdraw
 **   Mutator; This function decreases the balance, depending upon the amount
 **   of funds.
 **---------------------------------------------------------------------
 **   Parameter: withdrawAmt, accntNumber, and lastAccessed
 **---------------------------------------------------------------------
 **   Returns: bool if found
 ***********************************************************************/

/***********************************************************************
 ** UpdateAccount
 **   Mutator; This function updates all the information in the
 **   account
 **---------------------------------------------------------------------
 **   Parameter: none
 **---------------------------------------------------------------------
 **   Returns: nothing
 ***********************************************************************/

/***********************************************************************
 ** Deposit
 **   Mutator; This function changes the balance in the account (increases)
 **---------------------------------------------------------------------
 **   Parameter: amountIn
 **---------------------------------------------------------------------
 **   Returns: Nothing
 ***********************************************************************/

/***************
** ACCESSORS **
 ***************/

/***********************************************************************
 ** IsOverdrawn
 **   Mutator; gets true or false if the account is under $-200
 **---------------------------------------------------------------------
 **   Parameter: none
 **---------------------------------------------------------------------
 **   Returns: true or false
 ***********************************************************************/

/***********************************************************************
 ** IsNegative
 **   Mutator; gets true or false if the account is under $0
 **---------------------------------------------------------------------
 **   Parameter: none
 **---------------------------------------------------------------------
 **   Returns: true or false
 ***********************************************************************/

/***********************************************************************
 ** GetBalance
 **   Mutator; gets the account balance
 **---------------------------------------------------------------------
 **   Parameter: none
 **---------------------------------------------------------------------
 **   Returns: the account balance
 ***********************************************************************/

#endif /* CHECKING_H_ */
