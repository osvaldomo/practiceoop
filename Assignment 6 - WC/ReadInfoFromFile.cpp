/*****************************************************
 *AUTHOR         :  The Oz
 *CLASS          :  CS 1B
 *SECTION        :  TTh:  8am - 11:50am
 *DUE DATE       :  5/14/15
 *****************************************************/

#include "myHeader.h"
#include "SaddlebackBank.h"
#include "Date.h"
#include "Account.h"
#include "Savings.h"
#include "Checking.h"
#include "MoneyMarket.h"

/**********************************************************
*
* FUNCTION ReadInfoFromFile
*_________________________________________________________
* This function reads the account information from an
* input file. After reading the information, it creates a
* object depending upon  the account type.
*_________________________________________________________
* PRE-CONDITIONS
* createAccount  :  Account to be created
* inFileName     :  input file name
*
* POST-CONDITIONS
* This function will create a new Account type (savings,
* checking, or Money Market).
***********************************************************/

void ReadInfoFromFile(Bank &myBank,//IN/OUT - new account
					  string inFileName, //IN     - input file name
					  ostream &oFile)
{

	ifstream 	  inFile;  //IN - input stream

	Date           newDate;//IN - account creation date

			Savings *mySavings;
			MoneyMarket *myMoneyMarket;
			Checking *myChecking;
			Account *myAccount;

			unsigned short newDay;   //IN - account creation day
			unsigned short newMonth; //IN - account creation month
			unsigned short newYear;  //IN - account creation year

			unsigned short acctNumber; //IN - Account number
			string         acctType;   //IN - Account type
			float          balanceAmt; //IN - Account balance
			string         accntName;  //IN - Account owner's name

			char newAccType;

			//open input file
			inFile.open(inFileName.c_str());

			//while there is data in the file
			while(!inFile.eof())
			{

				inFile >> newDay;

				inFile >> newMonth;

				inFile >> newYear;

				inFile >> acctNumber;

				inFile >> acctType;

				inFile >> balanceAmt;

				inFile.ignore(numeric_limits <streamsize> :: max(), ' ');

				getline(inFile, accntName);

				newDate.SetDate(newDay, newMonth, newYear);

				if(acctType == "Checking")
				{
					myChecking = new Checking(accntName, acctNumber,
											  newDate, balanceAmt, acctType);
					myAccount = myChecking;
					newAccType =  'C';

				}
				else if(acctType == "Savings")
				{
					mySavings = new Savings(accntName, acctNumber, newDate,
											balanceAmt, acctType);
					myAccount = mySavings;
					newAccType = 'S';
				}
				else
				{
					myMoneyMarket = new MoneyMarket(accntName, acctNumber, newDate, balanceAmt, acctType);
					myAccount = myMoneyMarket;
					newAccType = 'M';
				}

				myBank.OpenAccount(myAccount);

				oFile << left;
				oFile << myAccount -> Display('N', 0, myAccount->GetBalance(), newAccType, newDate, 0.0);
							}

			oFile << endl;
}
